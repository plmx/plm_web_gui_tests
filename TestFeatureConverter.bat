echo 2>"C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Test_data_folder_name_list.txt"
echo 2>"C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Feature_folder_name_list.txt"

cd C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\test-data

dir /s /b /o:n /ad > C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Test_data_folder_name_list.txt
dir /s /b /o:n /ad > C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Feature_folder_name_list.txt
dir /s /b /o:n /ad > C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Pysrc_Folder_list_tmp.txt

cd C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC

@echo off 
    setlocal enableextensions disabledelayedexpansion

    set "search=test-data"
    set "replace=py-src\cucumber"

    set "textFile=Feature_folder_name_list.txt"

    for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
        set "line=%%i"
        setlocal enabledelayedexpansion
        >>"%textFile%" echo(!line:%search%=%replace%!
        endlocal
    )
	
@echo off 
    setlocal enableextensions disabledelayedexpansion

    set "search=test-data"
    set "replace=py-src"

    set "textFile=Pysrc_Folder_list_tmp.txt"

    for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
        set "line=%%i"
        setlocal enabledelayedexpansion
        >>"%textFile%" echo(!line:%search%=%replace%!
        endlocal
    )
	
	
	
FOR /F "tokens=* delims=" %%x in (C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Feature_folder_name_list.txt) DO if not exist %%x mkdir %%x
FOR /F "tokens=* delims=" %%x in (C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Pysrc_Folder_list_tmp.txt) DO if not exist %%x mkdir %%x


@echo off
setlocal EnableDelayedExpansion

rem Load first file into A1 array:
set i=0
for /F "delims=" %%a in (Test_data_folder_name_list.txt) do (
    set /A i+=1
    set A1[!i!]=%%a
)

rem Load second file into A2 array:
set i=0
for /F "delims=" %%a in (Feature_folder_name_list.txt) do (
    set /A i+=1
    set A2[!i!]=%%a
)

rem At this point, the number of lines is in %i% variable

rem Merge data from both files and create the third one:
for /L %%i in (1,1,%i%) do move !A1[%%i]!\*.feature !A2[%%i]!








REM ##################################################################################################
REM Replace below path:
REM 
REM 1) 
REM 
REM C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC
REM 
REM with
REM 
REM JLR Scripts path
REM 
REM 2) 
REM 
REM C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src\cucumber
REM 
REM with
REM 
REM JLR Features file path
REM 
REM 3)
REM 
REM C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src
REM 
REM with
REM 
REM JLR py-src path file path
REM ##################################################################################################

REM : resetting file_name_list.txt with zero data

@echo off

set cnt=0

echo 2>"C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\file_name_list.txt"

REM : With below command we are traversing or scanning to features folder and generating list of feature file names into file_name_list.txt file

for /R "C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src\cucumber" %%I in ("*.feature") do set /a cnt+=1 && echo %%I >> "C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\file_name_list.txt"

REM : Below block will search .feature string in file_name_list.txt and replace it will null

cd "C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\"

@echo off 
    setlocal enableextensions disabledelayedexpansion

    set "search=.feature "
    set "replace="

    set "textFile=file_name_list.txt"

    for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
        set "line=%%i"
        setlocal enabledelayedexpansion
        >>"%textFile%" echo(!line:%search%=%replace%!
        endlocal
    )

	
REM : Below code will generate .py file from feature file using pytest-bdd with timestamp like file_name_HH-MM-SS.MS.py 
set timestmp=%TIME::=-%
REM FOR /F "tokens=* delims=" %%x in (file_name_list.txt) DO pytest-bdd generate %%x.feature>%%x_%timestmp%.py && echo Feature File %%x have been converted to %%x.py file

FOR /F "tokens=* delims=" %%x in (file_name_list.txt) DO pytest-bdd generate %%x.feature>%%x_%timestmp%.py && echo Feature File %%x have been converted to %%x.py file



echo Total number of files converted : %cnt%

REM : resetting below temp files with zero data

echo 2>"C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Folder_name_list.txt"

echo 2>"C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\file_name_list_tmp.txt"

echo 2>"C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Pysrc_Folder_list_tmp.txt"

REM : Inserting list of folders present in C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src\cucumber into Folder_name_list.txt

cd C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src\cucumber

dir /a:d /b > C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Folder_name_list.txt

REM : Creating the folders which were identified in feature folder path into Py-src folder path

cd C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src
FOR /F "tokens=* delims=" %%x in (C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Folder_name_list.txt) DO if not exist %%x mkdir %%x

REM : We are creating file_name_list_tmp.txt which will hold the .py file names which we need to move from feature folder to pysrc folder

for /R "C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src\cucumber" %%I in ("*.feature") do set /a cnt+=1 && echo %%I >> "C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\file_name_list_tmp.txt"

REM : For moving .py files syntax is move <feature_path> <py file names> so we need feature path also and that will be generated in Pysrc_Folder_list_tmp.txt


REM REM for /f "delims=" %%D in ('dir /a:d /b') do echo %%~fD>> C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Pysrc_Folder_list_tmp.txt

cd C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src
dir /s /b /o:n /ad > C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\Pysrc_Folder_list_tmp.txt


REM 
REM @echo off 
REM     setlocal enableextensions disabledelayedexpansion
REM 
REM     set "search=C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\py-src\cucumber"
REM     set "replace="
REM 
REM     set "textFile=Pysrc_Folder_list_tmp.txt"
REM 
REM     for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
REM         set "line=%%i"
REM         setlocal enabledelayedexpansion
REM         >>"%textFile%" echo(!line:%search%=%replace%!
REM         endlocal
REM     )
REM 
REM for /f "usebackq tokens=* delims=" %%a in ("Pysrc_Folder_list_tmp.txt") do (echo(%%a)>>~.txt
REM move /y  ~.txt "Pysrc_Folder_list_tmp.txt"

REM : Below block will search .feature string in file_name_list_tmp.txt and replace it will null

cd C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC

FINDSTR /v /r "cucumber" Pysrc_Folder_list_tmp.txt > Pysrc_Folder_list_tmp2.txt

@echo off 
    setlocal enableextensions disabledelayedexpansion

    set "search=.feature "
    set "replace="

    set "textFile=file_name_list_tmp.txt"

    for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
        set "line=%%i"
        setlocal enabledelayedexpansion
        >>"%textFile%" echo(!line:%search%=%replace%!
        endlocal
    )



REM : With below block we are moving the .py files present in feature folder to pysrc folder	

@echo off
setlocal EnableDelayedExpansion

rem Load first file into A1 array:
set i=0
for /F "delims=" %%a in (file_name_list_tmp.txt) do (
    set /A i+=1
    set A1[!i!]=%%a
)

rem Load second file into A2 array:
set i=0
for /F "delims=" %%a in (Pysrc_Folder_list_tmp2.txt) do (
    set /A i+=1
    set A2[!i!]=%%a
)

rem At this point, the number of lines is in %i% variable

rem Merge data from both files and create the third one:
for /L %%i in (1,1,%i%) do move !A1[%%i]!_%timestmp%.py !A2[%%i]!

cd C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC

set cnt_feature=0

FOR /F "tokens=* delims=" %%x in (file_name_list.txt) DO set /a cnt_feature+=1 && del %%x.feature && echo %%x deleted from py-src\cucumber folder
echo 
echo 
echo Total feature files deleted from py-src\cucumber folder : %cnt_feature%

del C:\gitlab\plmx\plm_web_gui_tests\tree\PLM-1295_Test_Eng_POC\*.txt