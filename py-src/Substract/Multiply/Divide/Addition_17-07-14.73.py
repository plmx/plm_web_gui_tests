# coding=utf-8
"""Multiplication feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('Divide\Addition.feature', 'multiply a and b')
def test_multiply_a_and_b():
    """multiply a and b."""


@given('I have variable a')
def i_have_variable_a():
    """I have variable a."""
    raise NotImplementedError


@given('I have variable b
When I multiplication a and b
Then I display the Result')
def i_have_variable_bwhen_i_multiplication_a_and_bthen_i_display_the_result():
    """I have variable b
When I multiplication a and b
Then I display the Result."""
    raise NotImplementedError

