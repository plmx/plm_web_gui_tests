# coding=utf-8
"""guru99 Demopage Login feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('Substract\2222.feature', 'Register On Guru99 Demopage with valid email')
def test_register_on_guru99_demopage_with_valid_email():
    """Register On Guru99 Demopage with valid email."""


@scenario('Substract\2222.feature', 'Register On Guru99 Demopage without email')
def test_register_on_guru99_demopage_without_email():
    """Register On Guru99 Demopage without email."""


@given('I am on the Guru99 homepage')
def i_am_on_the_guru99_homepage():
    """I am on the Guru99 homepage."""
    raise NotImplementedError


@when('enter blank details for Register')
def enter_blank_details_for_register():
    """enter blank details for Register."""
    raise NotImplementedError


@when('enter details for Register')
def enter_details_for_register():
    """enter details for Register."""
    raise NotImplementedError


@then('error email shown')
def error_email_shown():
    """error email shown."""
    raise NotImplementedError


@then('login details shown')
def login_details_shown():
    """login details shown."""
    raise NotImplementedError

