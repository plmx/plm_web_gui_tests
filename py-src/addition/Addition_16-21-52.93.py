# coding=utf-8
"""Addition feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('addition\Addition.feature', 'Add two numbers')
def test_add_two_numbers():
    """Add two numbers."""


@given('I have entered <input_1> into the calculator')
def i_have_entered_input_1_into_the_calculator():
    """I have entered <input_1> into the calculator."""
    raise NotImplementedError


@given('I have entered <input_2> into the calculator')
def i_have_entered_input_2_into_the_calculator():
    """I have entered <input_2> into the calculator."""
    raise NotImplementedError


@when('I press <button>')
def i_press_button():
    """I press <button>."""
    raise NotImplementedError


@then('the result should be <output> on the screen')
def the_result_should_be_output_on_the_screen():
    """the result should be <output> on the screen."""
    raise NotImplementedError

