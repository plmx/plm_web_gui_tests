 |Action       | Description           | Apporch |Method Name |Location |
| ------------- |:-------------:| -----:| -----:|-----:|
| create issue   | Issue Creation method Returns Issue name|selenium|create_issue() |plm_web_gui_tests \ py-src\ selenium_helper_methods\ create_issue.py
| create workspace folder |   Workspace creation method Returns workspace folder name.  | selenium   | create_workspace_folder()   |plm_web_gui_tests \py-src\ selenium_helper_methods\  create_workspace_folder.py
| login|  for logging into the platform through the web browser  |  selenium  | login(url, user, passwd)|plm_web_gui_tests \py-src\ selenium_helper_methods\ login_web.py
| delete vpm data|  This deletion mechanism uses Product Structure Explorer and PAU licenese to   |  selenium  | delete_vpm_data(object_name)|plm_web_gui_tests \py-src\ selenium_helper_methods\ data_deletion.py 
| is static|  This method finds an element an loops until the position of the element is static  |  selenium  | is_static(locate_by, search_locator)|plm_web_gui_tests \py-src\ selenium_helper_methods\dynamic_element .py   
| wait for change| Method used for actions which trigger an attribute change (i.e. Tab creation, Tab switching, etc) Method recalculates the value of an specified attribute for a given element.Compares this value to the initial value provided as argument. |  selenium  | wait_for_change(initial_attribute_value, locate_by, search_locator, attribute_name)|plm_web_gui_tests \py-src\ selenium_helper_methods\dynamic_element .py    
| wait for element to be stale| Checks for an element to become stale. Catches exception, reloaded and returns element  |  selenium  | wait_for_element_to_be_stale(locate_by, search_locator)|plm_web_gui_tests \py-src\ selenium_helper_methods\dynamic_element .py     
| region grabber|grabs region to the tuple | pyautogui |region_grabber|plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|imagesearcharea|Searchs for an image within an area|pyautogui|imagesearcharea|plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|click image|click on the center of an image with a bit of random.|pyautogui|click_image|plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|imagesearch|Searchs for an image on the screen|pyautogui|imagesearch|plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|imagesearch loop|Searchs for an image on screen continuously until it's found.|pyautogui|imagesearch_loop|plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|imagesearch numLoop|Searchs for an image on screen continuously until it's found or max number of samples reached.|pyautogui|imagesearch_ numLoop|plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|imagesearch region loop|Searchs for an image on a region of the screen continuously until it's found.|pyautogui|imagesearch_ region_loop|plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|imagesearch & count|Searches for an image on the screen and counts the number of occurrences.|pyautogui|imagesearch_count|plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|undefined|undefined|undefined| r |plm_web_gui_tests \ py-src\ native_app_helper_methods\ image_search.py
|connect by class name|This method will try to connect to an desktop application by unsing its class name If connected, it will return handle to application. None otherwise|pywinauto selenium |connect_by_ class_name|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|pin tree view|This method is used to pin the tree view|pyautogui|_pin_tree_view|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|extend tree view|This method is used to extend the tree view|pyautogui|_extend_tree_view|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|open_BI_Essential|This method is used to open the B.I. Essentials|pyautogui selenium|open_BI_Essential|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|command to power input|This method is used to run the commands from Power input|pyautogui selenium|_command_to_power_input|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|find and select|This method is used to find the Part in the tree|pywinauto selenium|_find_and_select|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|switch to instance tab|This method is used to switch to instance tab|pyautogui|_switch_to_instance_tab|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|switch to cad tab|This method is used to switch to instance tab|pyautogui|_switch_to_cad_tab|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|search and explore from driver|This method is used to search from webtop and open to Explore|selenium pywinauto|_search_and_ explore_from_driver|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|close|Close 3DEXPERIENCE|pywinauto|close|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|close graphic driver and error message|  This method is used to close the error message dialog that pops up when graphic driver is not supported by DS|pywinauto|close_graphic_ driver_error_message|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|get quadrant|Get quadrants of the screen. This will make image search faster by reducing the area to look for.|pyautogui|get_quadrant|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|launch app into native| Launchs selective app into native client by opening the compass and going through the user procedure|selenium pywinauto|launch_app_ into_native|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|wait for window to close|Wait for window to close|selenium pywinauto|wait_for_window_to_close|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|create native app object|This method is used create object in native_app|pywinauto|_create_native _app_object|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|close most used entities|Undefined|pywinauto|_close_most _used_entities|plm_web_gui_tests \ py-src\ native_app_helper_methods\ native_app.py
|drag and drop|Drags and drops an a search result from the results window onto the destination element|selenium pyautogui|drag_and_drop|plm_web_gui_tests \ py-src\selenium_helper_methods\ search_drag_and_drop.py
|reset result to list|undefined|selenium|reset_result_to_list|plm_web_gui_tests \ py-src\selenium_helper_methods\ search_drag_and_drop.py
|clear widget view|Method to clear view if some data is already loaded in the widget|selenium|clear_widget_view|plm_web_gui_tests \ py-src\selenium_helper_methods\ search_drag_and_drop.py
|verify search results|Method to verifies if search data is visible on search results|selenium|verify_search_results|plm_web_gui_tests \ py-src\selenium_helper_methods\ six_w_tags_actions.py
|click filter stale elements|undefined|selenium|click_filter_stale_elements|plm_web_gui_tests \ py-src\selenium_helper_methods\ six_w_tags_actions.py
|top toolbar search|Method to enter text to be searched in search top tool bar located the search input, clears the contents and then inputs end executes the search|selenium|top_toolbar_search|plm_web_gui_tests \ py-src\selenium_helper_methods\ six_w_tags_actions.py
|validate part reuse exists|This method is used to validate Part reuse attribute|pywinauto selenium|validate_part_reuse_exists|plm_web_gui_tests \ py-src\ native_app_helper_methods\ part_reuse.py
|set reuse value|This method is used to set the reuse Value to In_Work Data|pywinauto|set_reuse_value|plm_web_gui_tests \ py-src\ native_app_helper_methods\ part_reuse.py
|verify reuse value|This method is used to verify if the value is set|pywinauto|_verify_reuse_value|plm_web_gui_tests \ py-src\ native_app_helper_methods\ part_reuse.py
|reset reuse value|This method is used to set reset the reuse value|pywinauto|_reset_reuse_value|plm_web_gui_tests \ py-src\ native_app_helper_methods\ part_reuse.py
|validate frozen part reuse|This method is used to validate edit of Part Reuse for Frozen Part Instance|pywinauto selenium|validate_frozen_part_reuse|plm_web_gui_tests \ py-src\ native_app_helper_methods\ part_reuse.py
|validate colour wash|undefined|pyautogui|validate_colour_wash|plm_web_gui_tests \ py-src\ native_app_helper_methods\ part_reuse.py
|verify part reuse first use|This method is used to validate Part reuse and first use program attribute within the CAD page|selenium pywinauto|verify_part_reuse_first_use|plm_web_gui_tests \ py-src\ native_app_helper_methods\ part_reuse.py
|verify model year|This method will check the model year is visible in edit box, this may need modifying to verify when model year data is entered i.e. 2020|pywinauto|verify_model_year|plm_web_gui_tests \ py-src\ native_app_helper_methods\ part_reuse.py
|Get chrome driver  | Set the initial directory |  | get_chrome_driver |plm_web_gui_tests \py-src\ selenium_helper_methods\load_chrome_driver .py   |
|Configure options  |Set the Chrome Driver options to prevent error popup returns option |  | configure_options |plm_web_gui_tests \py-src\ selenium_helper_methods\load_chrome_driver .py      | |
|Find chrome driver | Moves up a directory at a time from the target path until it is in a folder that contains the folder called "driver" and then sets the driver up. |  |find_chrome_driver  | plm_web_gui_tests \py-src\ selenium_helper_methods\load_chrome_driver .py     |
|create from add menu|creates an object from the add menu of the top toolbar in the web client.  |  |create_from_add_menu |plm_web_gui_tests \\py-src\selenium_helper_methods\menu_navigation.py
|click add buttonelement|creates an object from the add menu of the top toolbar in the web client|  |pyautogui.click  | plm_web_gui_tests \\py-src\selenium_helper_methods\menu_navigation.py |
|Move To menu element  |creates an object from the add menu of the top toolbar in the web client.|  | pyautogui.moveTo  |plm_web_gui_tests \\py-src\selenium_helper_methods\menu_navigation.py  |
|open app from compass|Launch an app form the compass menu  |  |open_app_from_compass | plm_web_gui_tests \\py-src\selenium_helper_methods\menu_navigation.py |
|open dashboardwgt app from compass|Launch an app form the compass menu. |  |open_dashboard_wgt_app_from_compass|plm_web_gui_tests \\py-src\selenium_helper_methods\menu_navigation.py |
|position web element |Converts selenium screen positon coordinates to global screen position coordinates for Pyautogui|  |selenium2pyautogui_position |  plm_web_gui_tests \py-src\ selenium_helper_methods\pyautogui_helper.py |
|screen positon coordinates to selenium screen | Converts PyAutoGui screen positon coordinates to selenium screen position coordinates |  | pyautogui2selenium_position  |plm_web_gui_tests \py-src\ selenium_helper_methods\pyautogui_helper.py   |
|create |Creates a collaborative task in the collaborative tasks widget|  | create|plm_web_gui_tests \py-src\selenium_helper_methods\collab_|
|delete |Takes task name argument, deletes task, verifies deletion success message is displayed. Returns result - True or False  |  |delete  | plm_web_gui_tests \py-src\selenium_helper_methods\collab_ |
|assign task|  |  | assign_task |plm_web_gui_tests \py-src\selenium_helper_methods\collab_  |
|delete assignee|his method will remove a assigned task from a assignee  |  |delete_assignee  |plm_web_gui_tests \py-src\selenium_helper_methods\collab_  |
|add attachment| This method will add the attachment on task |    | add_attachment | |
|add attachment drag_and drop |This method will add the attachment by drag and drop from search results   |  |add_attachment_drag_and_drop |plm_web_gui_tests \py-src\selenium_helper_methods\collab_ |
|add attachment from autofill| This method will add the attachment by type ahead on attachment field  |  |add_attachment_from_autofill  | plm_web_gui_tests \py-src\selenium_helper_methods\collab_|
|move to attachmentfield|This method will move to attachment field|  |move_to_attachment_field  |plm_web_gui_tests \py-src\selenium_helper_methods\collab_   |
|delete attachment |This method will delete all the attachments from the task"|  |delete_attachment  |plm_web_gui_tests \py-src\selenium_helper_methods\collab_|
|add descr |This selects a task, finds the description field, clears the content and inputs new description |  |add_descr |plm_web_gui_tests \py-src\selenium_helper_methods\collab_|
|change lanes |This method changes the lane of a collaborative task to a different maturity state|  |change_lanes|plm_web_gui_tests \py-src\selenium_helper_methods\collab_ |
|switch to task|This method switch to the task view|  |switch_to_task |\master\py-src\selenium_helper_methods|
|save task|  |  |save_task|\master\py-src\selenium_helper_methods|
|switch to collab task frame|This switches to the collaborative tasks frame. Remember to return to default frame afterwards|  |switch_to_collab_task_frame|plm_web_gui_tests \py-src\selenium_helper_methods\collab_|
|Return to default frame|  |  |return_to_default_frame |plm_web_gui_tests \py-src\selenium_helper_methods\collab_ |
|drag app to dashboard|Drags an app to the dashboard|pyautogui,selenium|drag_app_to_dashboard|plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py| 
|create_dashboard|Creates a new dashboard Loads Dashboard id before and after to ensure new dashboard has been created|pyautogui,selenium |create_dashboard|plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py |
|create tab|Helper method to create tab in the "current" dash board Loads tab id before and after to ensure new tab has been created.|pyautogui,selenium|create_tab  |plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py |
|delete current dashboard |Deletes the current dashboard - must already be in the dashboard to perform this  |pyautogui,selenium|delete_current_dashboard  |plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py|
|delete(dashboard_name)|Switches into dashboard in given as argument and deletes it.  |pyautogui,selenium|delete|plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py  |
|validate dashboard_widgets|This method lists the Widgets to be searched in dashboard|pyautogui,selenium|validate_dashboard_widgets|plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py |
|validate widgets exists|This method needs the list of Widgets to be searched and return success |pyautogui,selenium|validate_widgets_exists|plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py|
|switch to tab|This method need the tab name to switch|pyautogui,selenium|switch_to_tab |plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py|
|switch to dashboard|Switch to a desired dashboard  |pyautogui,selenium| switch_to_dashboard |plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py|
|assert is displayed(tab_name)  |  |pyautogui,selenium|assert_is_displayed  |plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py |
|maximise_widget(widget_qualifier)  | Method to maximise the Collaborative Task widget  |pyautogui,selenium|maximise_widget  |plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py|
|restore widget  |Method to Restore the Collaborative Task widget  |pyautogui,selenium|restore_widget  |plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py|
|resize_widget(widget_qualifier) |Method to restore the Collaborative Task widget |pyautogui,selenium|resize_widget  |plm_web_gui_tests \py-src\ selenium_helper_methods\dashboard_actions.py|


